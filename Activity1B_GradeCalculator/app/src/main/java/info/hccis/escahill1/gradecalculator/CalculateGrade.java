package info.hccis.escahill1.gradecalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class CalculateGrade extends AppCompatActivity {

    private final double PROJECT_WEIGHT = 0.8;
    private final double RESEARCH_WEIGHT = 0.2;

    private double projectGrade = 0.0;
    private double researchGrade = 0.0;
    private TextView gradeValue = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_grade);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        Bundle extras = getIntent().getExtras();
//        projectGrade = extras.getDouble("projectGrade");
//        researchGrade = extras.getDouble("researchGrade");

//        Bundle extras = getIntent().getExtras();
        projectGrade = getIntent().getDoubleExtra("projectGrade",0.0);
        researchGrade = getIntent().getDoubleExtra("researchGrade", 0.0);

        gradeValue = (TextView) findViewById(R.id.gradeValue);

        Double finalGrade = calculateGrade(projectGrade, researchGrade);
        gradeValue.setText(finalGrade.toString());

    }

    /**
     * Method calculates the final grade based on the project and research grade values received from
     * the Intent.
     *
     * @author Evan Cahill
     * @since 14-Jan-2016
     *
     * @return
     */
    private double calculateGrade(double projectGrade, double researchGrade) {
        double finalGrade = 0;

        // calculate grade
        finalGrade = (PROJECT_WEIGHT * projectGrade) + (RESEARCH_WEIGHT * researchGrade);

        return finalGrade;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_restart) {
            Log.d("menu", "restart was chosen");

            Intent intent = new Intent(CalculateGrade.this, MainActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
