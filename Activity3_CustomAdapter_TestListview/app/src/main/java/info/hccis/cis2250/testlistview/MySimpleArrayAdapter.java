package info.hccis.cis2250.testlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MySimpleArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public MySimpleArrayAdapter(Context context, String[] values) {
        super(context, R.layout.row_layout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_layout, parent, false);

        TextView textView = (TextView) rowView.findViewById(R.id.label);
        TextView textViewSub = (TextView) rowView.findViewById(R.id.sublabel);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        //BJM
        // Set the labels on the text views.

        textView.setText(values[position]);
        textViewSub.setText("This is the sub-label for "+position);


        // change the icon for Windows and iPhone
        // BJM
        // Have to change the drawables in this project for this to work.
        // They are all set the same.
        String s = values[position];
        if (s.startsWith("iPhone")) {
            imageView.setImageResource(R.drawable.no);
        } else {
            imageView.setImageResource(R.drawable.ok);
        }

        return rowView;
    }
}