package info.hccis.cis2250.testactivities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Activity1 extends ActionBarActivity {

    private EditText editText1 = null;
    private Button button1 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity1);

        //Associate with the widgets.
        editText1 = (EditText) findViewById(R.id.editText1);
        button1 = (Button) findViewById((R.id.button1));

        button1.setOnClickListener(new View.OnClickListener() {
                                       public void onClick(View view) {
                                           Log.d("BJTEST", "The button was clicked");
                                           EditText editText1 = (EditText) findViewById(R.id.editText1);
                                           Log.d("BJTEST", "The button was clicked and the user types:"+editText1.getText().toString());
                                           Intent intent = new Intent(Activity1.this, Activity2.class);
                                           startActivity(intent);
                                           finish();

                                       }
                                   }
        );
        Log.d("BJTEST", "onCreate run in Activity 1");
    }




    @Override
    protected void onStart(){
        super.onStart();
        Log.d("BJTEST", "onStart was run in Activity 1");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d("BJTEST", "onResume was run in Activity 1");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.d("BJTEST", "onPause was run in Activity 1");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("BJTEST", "onStop was run in Activity 1");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("BJTEST", "onDestroy was run in Activity 1");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
