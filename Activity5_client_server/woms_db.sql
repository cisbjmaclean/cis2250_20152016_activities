-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2015 at 09:36 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `woms_db`
--
CREATE DATABASE IF NOT EXISTS `woms_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `woms_db`;

-- --------------------------------------------------------

--
-- Table structure for table `authority`
--
DROP TABLE IF EXISTS `authority`;
CREATE TABLE IF NOT EXISTS `authority` (
  `AuthorityCode` int(3) NOT NULL,
  `Description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authority`
--

INSERT INTO `authority` (`AuthorityCode`, `Description`) VALUES
(1, 'Administrator'),
(2, 'Support Staff'),
(3, 'Regular User');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `CategoryID` int(3) NOT NULL,
  `DeptCode` int(3) NOT NULL,
  `Description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `CommentID` int(7) NOT NULL,
  `Content` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
CREATE TABLE IF NOT EXISTS `department` (
  `DeptCode` int(3) NOT NULL,
  `DeptDescription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `knowledgebase`
--

DROP TABLE IF EXISTS `knowledgebase`;
CREATE TABLE IF NOT EXISTS `knowledgebase` (
  `TicketNum` int(9) NOT NULL,
  `CategoryID` int(3) NOT NULL,
  `SearchTermID` int(5) NOT NULL,
  `Issue` varchar(5000) NOT NULL,
  `Solution` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
CREATE TABLE IF NOT EXISTS `room` (
  `RoomNum` int(5) NOT NULL,
  `Description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `searchterm`
--

DROP TABLE IF EXISTS `searchterm`;
CREATE TABLE IF NOT EXISTS `searchterm` (
  `SearchTermID` int(5) NOT NULL,
  `Description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supportstaff`
--

DROP TABLE IF EXISTS `supportstaff`;
CREATE TABLE IF NOT EXISTS `supportstaff` (
  `UserNum` int(7) NOT NULL,
  `DeptCode` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
`TicketNum` int(9) NOT NULL,
  `DeptCode` int(3) NOT NULL,
  `CategoryID` int(3) NOT NULL,
  `TicketStatusCode` int(2) NOT NULL,
  `RoomNum` int(5) NOT NULL,
  `TicketPriorityCode` int(7) NOT NULL,
  `TicketOpen` datetime NOT NULL,
  `TicketClose` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticketstatus`
--

DROP TABLE IF EXISTS `ticketstatus`;
CREATE TABLE IF NOT EXISTS `ticketstatus` (
  `TicketStatusCode` int(2) NOT NULL,
  `Description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
`UserNum` int(11) NOT NULL,
  `UserEmail` varchar(50) NOT NULL,
  `UserPassword` varchar(50) NOT NULL,
  `UserLastName` varchar(24) NOT NULL,
  `UserFirstName` varchar(24) NOT NULL,
  `AuthorityCode` int(3) NOT NULL,
  `UserStatus` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--
INSERT INTO `user` (`UserNum`, `UserEmail`, `UserPassword`, `UserLastName`, `UserFirstName`, `AuthorityCode`, `UserStatus`) VALUES
(1, 'admin@hollandcollege.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Admin', 'Marl', 1, 1),
(2, 'andrew', '02e0a999c50b1f88df7a8f5a04e1b76b35ea6a88', 'andrew', 'andrew', 1, 1),
(3, 'user', 'pass', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `userticket`
--

DROP TABLE IF EXISTS `userticket`;
CREATE TABLE IF NOT EXISTS `userticket` (
  `UserNum` int(7) NOT NULL,
  `TicketNum` int(9) NOT NULL,
  `CommentID` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authority`
--
ALTER TABLE `authority`
 ADD PRIMARY KEY (`AuthorityCode`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`CategoryID`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
 ADD PRIMARY KEY (`CommentID`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`DeptCode`);

--
-- Indexes for table `knowledgebase`
--
ALTER TABLE `knowledgebase`
 ADD PRIMARY KEY (`TicketNum`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
 ADD PRIMARY KEY (`RoomNum`);

--
-- Indexes for table `searchterm`
--
ALTER TABLE `searchterm`
 ADD PRIMARY KEY (`SearchTermID`);

--
-- Indexes for table `supportstaff`
--
ALTER TABLE `supportstaff`
 ADD PRIMARY KEY (`UserNum`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
 ADD PRIMARY KEY (`TicketNum`);

--
-- Indexes for table `ticketstatus`
--
ALTER TABLE `ticketstatus`
 ADD PRIMARY KEY (`TicketStatusCode`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`UserNum`), ADD UNIQUE KEY `Email` (`UserEmail`);

--
-- Indexes for table `userticket`
--
ALTER TABLE `userticket`
 ADD PRIMARY KEY (`UserNum`,`TicketNum`,`CommentID`), ADD KEY `CommentID` (`CommentID`), ADD KEY `TicketNum` (`TicketNum`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
MODIFY `TicketNum` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `UserNum` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `knowledgebase`
--
ALTER TABLE `knowledgebase`
ADD CONSTRAINT `knowledgebase_ibfk_1` FOREIGN KEY (`TicketNum`) REFERENCES `ticket` (`TicketNum`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supportstaff`
--
ALTER TABLE `supportstaff`
ADD CONSTRAINT `supportstaff_ibfk_1` FOREIGN KEY (`UserNum`) REFERENCES `user` (`UserNum`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userticket`
--
ALTER TABLE `userticket`
ADD CONSTRAINT `userticket_ibfk_1` FOREIGN KEY (`UserNum`) REFERENCES `user` (`UserNum`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `userticket_ibfk_2` FOREIGN KEY (`CommentID`) REFERENCES `comment` (`CommentID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `userticket_ibfk_3` FOREIGN KEY (`TicketNum`) REFERENCES `ticket` (`TicketNum`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
