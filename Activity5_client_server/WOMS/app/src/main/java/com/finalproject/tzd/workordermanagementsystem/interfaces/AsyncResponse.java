package com.finalproject.tzd.workordermanagementsystem.interfaces;

import org.json.JSONObject;

// From HelmiB, toobsco42 http://stackoverflow.com

public interface AsyncResponse {
    void processFinish(JSONObject jsonResponse);
}