package com.finalproject.tzd.workordermanagementsystem.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.finalproject.tzd.workordermanagementsystem.R;
import com.finalproject.tzd.workordermanagementsystem.interfaces.AsyncResponse;
import com.finalproject.tzd.workordermanagementsystem.network.JSONPost;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


//Taken from https://mongskiewl.wordpress.com/2013/10/16/sending-json-data-from-android-to-a-php-script/


public class LoginActivity extends ActionBarActivity implements AsyncResponse {

    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //This method takes the login details entered by the user, sends them off to be validated,
    // and logs them in if they are valid
    public void loginUser(View view) {
        username = (EditText) findViewById(R.id.usernameText);
        password = (EditText) findViewById(R.id.passwordText);

        String email = username.getText().toString();
        String pass = password.getText().toString();
        //Hashing the password using SHA1 Encryption
//        try {
//            pass = sha1Encode(password.getText().toString());
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }


        // Use AsyncTask execute Method To Prevent ANR Problem
        try {
            JSONObject login = new JSONObject();
            login.put("username", email);
            login.put("password",pass);

            //JSONPost loginConnect = new JSONPost("http://192.168.0.3/DAO/login.php");
            JSONPost loginConnect = new JSONPost("http://bjmac2.hccis.info:80/DAO/login.php");

            // From HelmiB, toobsco42 http://stackoverflow.com
            loginConnect.delegate = this;

            loginConnect.execute(new JSONObject[] {login});

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Part from From HelmiB, toobsco42 http://stackoverflow.com
    @Override
    public void processFinish(JSONObject jsonResponse)
    {
        try {
            // From From HelmiB, toobsco42 http://stackoverflow.com
            JSONObject loginDetails;
            String temp = jsonResponse.getString("response");
            Log.d("BJTEST LoginActivity temp",temp);
            loginDetails = new JSONObject(temp);

        // Forward to correct screen
        if(loginDetails.getInt("loginStatus") == 1){
            Intent i = new Intent(LoginActivity.this, MainMenuActivity.class);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(this, "Incorrect Login Details", Toast.LENGTH_LONG).show();
        }
        }catch (Exception e) { e.printStackTrace();}
    }

    //Built from https://www.youtube.com/watch?v=KPJXecFdEDo video tutorial
    //This method Accepts a string for a password, encrypts it into hex using SHA1, and returns the hex encoded string.
    private String sha1Encode(String password) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(password.getBytes());
        StringBuffer hashedPassword = new StringBuffer();

       for(int i=0; i<result.length; i++){
           hashedPassword.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
       }
        return hashedPassword.toString();
    }

}
