package com.finalproject.tzd.workordermanagementsystem.network;

import android.os.AsyncTask;
import android.util.Log;

import com.finalproject.tzd.workordermanagementsystem.interfaces.AsyncResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;

//Taken from https://mongskiewl.wordpress.com/2013/10/16/sending-json-data-from-android-to-a-php-script/

public class JSONPost extends AsyncTask<JSONObject, JSONObject, JSONObject> {

    private String url = "";


    public AsyncResponse delegate = null;
    private JSONObject jsonResponse = null;

    public JSONPost (String url){
        this.url = url;
    }

    @Override
    protected void onPreExecute()
    {
    }

    @Override
    protected JSONObject doInBackground(JSONObject... data) {
        JSONObject json = data[0];
        Log.d("BJTEST json=",json.toString());
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);

        HttpPost post = new HttpPost(url);
        try {
            StringEntity se = new StringEntity("json="+json.toString());
            post.addHeader("content-type", "application/x-www-form-urlencoded");
            post.setEntity(se);

            HttpResponse response;
            response = client.execute(post);
            String resultFromServer = org.apache.http.util.EntityUtils.toString(response.getEntity());
            Log.d("BJTEST1", resultFromServer);
            jsonResponse = new JSONObject(resultFromServer);
        } catch (Exception e) { e.printStackTrace();}

        return jsonResponse;
    }

    // From HelmiB, toobsco42 http://stackoverflow.com
    @Override
    protected void onPostExecute(JSONObject jsonResponse) {
        try {
            delegate.processFinish(jsonResponse);
        } catch (Exception e) { e.printStackTrace();}
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}