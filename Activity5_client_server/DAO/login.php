<?php

include('db_connect.php');

if (isset($_POST["json"])) {
	$dataExpanded;
    $data = json_decode($_POST["json"], true);
  
    // Create the query.
   $stmt = $mysqli->prepare("SELECT UserNum, AuthorityCode FROM User WHERE UserEmail=? AND UserPassword=?");
    // Secure the statement against injection attacks.
    //$stmt->bind_param('ss', $username, $hashedPassword);
    $stmt->bind_param('ss', $userEmail, $password);

    // Get user variables and add security to password.
    $userEmail = trim($data['email']);
    //$hashedPassword = hash("sha256", trim($_POST['password']));
    $password = trim($data['password']);
// Execute query.
    $stmt->execute();
    // Used for counting the number of rows.
    $stmt->store_result();
    // Count rows.
    $rowCount = $stmt->num_rows;
    // Secure the result set.
    $stmt->bind_result($userNumResult, $userAuthorityResult);
    // Get the result array.
    $stmt->fetch();

    // If a user with the correct credentials is found, set their id for use in the system, else return them to the login page.
    $loginResponse = array();
    if ($rowCount == 1) {
       $loginResponse['loginStatus'] = 1;
       $loginResponse['userNum'] = $userNumResult ;
       $loginResponse['authorityCode'] = $userAuthorityResult;
    } else {
         $loginResponse['loginStatus'] = 0;
    }
    // Close statement.
    $stmt->close();
    // Close connection.
    $mysqli->close();
    $jsonResult = array();
    $jsonResult ['response'] = $loginResponse;
    echo(json_encode($jsonResult));
}
?> 