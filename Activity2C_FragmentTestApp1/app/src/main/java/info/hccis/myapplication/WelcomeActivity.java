package info.hccis.myapplication;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SpinnerAdapter;
import android.widget.Toast;


public class WelcomeActivity extends Activity {

//    public class WelcomeActivity extends ActionBarActivity implements ActionBar.OnNavigationListener {

//    android.app.ActionBar.OnNavigationListener onNavigationListener = new android.app.ActionBar.OnNavigationListener() {
//        // Get the same strings provided for the drop-down's ArrayAdapter
//        String[] strings = getResources().getStringArray(R.array.action_list);
//
//        @Override
//        public boolean onNavigationItemSelected(int position, long itemId) {
//            // Create new fragment from our own Fragment class
//            Toast.makeText(getApplicationContext(), "Navigation item selected now in onNavigationItemSelected", Toast.LENGTH_SHORT).show();
//            return true;
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
//        SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(this,
//        R.array.action_list, android.R.layout.simple_spinner_dropdown_item);
//        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
//        getActionBar().setListNavigationCallbacks(mSpinnerAdapter, onNavigationListener);
        showDialog(this, "View mode", "How do you want to view fragments?");
    }

    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Listview", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-Listview", Toast.LENGTH_LONG).show();
                startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
            }
        });
        builder.setNegativeButton("Swipe", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "selected-swipe", Toast.LENGTH_LONG).show();
                startActivity(new Intent(WelcomeActivity.this, SwipeActivity.class));
            }
        });
        builder.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_home) {
            Log.d("BJTEST", "Home was selected");
            Toast.makeText(getApplicationContext(), "Home was selected", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public boolean onNavigationItemSelected(int i, long l) {
//        Toast.makeText(getApplicationContext(), "Navigation item selected", Toast.LENGTH_SHORT).show();
//        return false;
//    }
}
