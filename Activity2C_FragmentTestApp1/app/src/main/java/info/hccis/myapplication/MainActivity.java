package info.hccis.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;


public class MainActivity extends Activity {

    FragmentManager  fm = this.getFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();
    FragmentNotImplemented fragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null) {
            fragment1 = new FragmentNotImplemented();
            ft.replace(R.id.fragment, new FragmentNotImplemented());
            ft.commit();
        }

        final ListView lv = (ListView) findViewById(R.id.listViewNames);
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                Object o = lv.getItemAtPosition(position);
//                Toast.makeText(getApplicationContext(),
//                        o.toString(), Toast.LENGTH_SHORT).show();

                String chosen = (String) o;
                Log.d("BJTEST", "chosen item from listview = " + chosen);

                Fragment newFragment = new FragmentNotImplemented();
//                fragment2.setName(chosen);


                    ft = fm.beginTransaction();
       //             Fragment1 fragment1 = new Fragment1();
                    ft.replace(R.id.fragment, newFragment);
                    ft.commit();
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}