package info.hccis.myapplication;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity implements FragmentTim.OnFragmentTimInteractionListener,
        FragmentAndrew.OnFragmentAndrewInteractionListener,
        FragmentLogan.OnFragmentLoganInteractionListener,
        FragmentPhilip.OnFragmentPhilipInteractionListener,
        FragmentJordan.OnFragmentJordanInteractionListener,
        FragmentJon.OnFragmentJonInteractionListener,
        FragmentRyanF.OnFragmentRyanFInteractionListener,
        FragmentMike.OnFragmentMikeInteractionListener,
        FragmentRobert.OnFragmentRobertInteractionListener{

    FragmentManager fm = this.getFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();
    FragmentNotImplemented fragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        fragment1 = new FragmentNotImplemented();
        ft.replace(R.id.fragment, new FragmentNotImplemented());
        ft.commit();

        final ListView lv = (ListView) findViewById(R.id.listViewNames);
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                Object o = lv.getItemAtPosition(position);
//                Toast.makeText(getApplicationContext(),
//                        o.toString(), Toast.LENGTH_SHORT).show();

                String chosen = (String) o;
                Log.d("BJTEST", "chosen item from listview = " + chosen);

                Fragment newFragment = new FragmentNotImplemented();
//                fragment2.setName(chosen);

                ft = fm.beginTransaction();

                switch (chosen) {
                    case "Phil":
                        newFragment = new FragmentPhilip();
                        break;
                    case "Logan":
                        newFragment = new FragmentLogan();
                        break;
                    case "Andrew":
                        newFragment = new FragmentAndrew();
                        break;
                    case "Roger":
                        newFragment = new FragmentRoger();
                        break;
//                    case "RyanM":
//                        newFragment = new FragmentRyanM();
//                        break;
                    case "Jordan":
                        newFragment = new FragmentJordan();
                        break;
                    case "Mike":
                        newFragment = new FragmentMike();
                        break;
//                    case "Nathan":
//                        newFragment = new FragmentNathan();
//                        break;
                    case "Jon":
                        newFragment = new FragmentJon();
                        break;
                    case "RyanF":
                        newFragment = new FragmentRyanF();
                        break;
                    case "Kylea":
                        newFragment = new FragmentKylea();
                        break;
                    case "Tim":
                        newFragment = new FragmentTim();
                        break;
                    case "Robert":
                        newFragment = new FragmentRobert();
                        break;
                    case "Jarred":
                        newFragment = new FragmentJarred();
                        break;

                }


//                    Fragment1 fragment1 = new Fragment1();
                ft.replace(R.id.fragment, newFragment);
                ft.commit();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFragmentAndrewInteraction(String message) {
        Log.d("BJTEST", "fragment 3 interaction happened...");
    }

    @Override
    public void onFragmentPhilipInteraction(String message) {
        Log.d("BJTEST", "fragment phil interaction happened...");
    }

    @Override
    public void onFragmentJordanInteraction(String message) {

    }

    @Override
    public void onFragmentJonInteraction(String message) {
        Log.d("BJTEST", "fragment Jon interaction happened...");
    }

    @Override
    public void onFragmentTimInteraction(Uri uri) {

    }

    @Override
    public void onFragmentRyanFInteraction(Uri uri) {

    }

    @Override
    public void onFragmentMikeInteraction(String message) {

    }

    @Override
    public void onFragmentLoganInteraction(String message) {

    }

    @Override
    public void onFragmentRobertInteraction(String message) {

    }
}
