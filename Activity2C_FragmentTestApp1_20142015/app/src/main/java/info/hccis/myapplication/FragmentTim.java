package info.hccis.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentTim.OnFragmentTimInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentTim#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentTim extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // Parameters
    private VideoView videoView;
    private int position = 0;
    private ProgressDialog progressDialog;
    private android.widget.MediaController mediaController;

    private OnFragmentTimInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentTim.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentTim newInstance(String param1, String param2) {
        FragmentTim fragment = new FragmentTim();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentTim() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //restore instance state, get stored position
        try{
            position = savedInstanceState.getInt("Position");
        }catch(Exception e){

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_tim, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        Activity act = this.getActivity();

        //init videoview
        videoView = (VideoView) act.findViewById(R.id.videoView);

        //set media controller
        if(mediaController == null){
            mediaController = new android.widget.MediaController(act);
        }

        //progress bar stuff
        progressDialog = new ProgressDialog(act);
        progressDialog.setTitle("Loading...");
        progressDialog.setMessage("");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try{
            //associate media controller with video view
            videoView.setMediaController(mediaController);

            //set location of video to be played
            String uri = "android.resource://" + act.getPackageName() + "/" + R.raw.waffle;
            videoView.setVideoURI(Uri.parse(uri));
        }catch(Exception e){
            Log.i("test", e.getMessage());
            e.printStackTrace();
        }

        //draw focus
        videoView.requestFocus();

        //onPrepared listener, checks when file is ready for playback
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                //close progress bar
                progressDialog.dismiss();

                //seek to last set video position
                videoView.seekTo(position);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);

        //pause video, store current position
        savedInstanceState.putInt("Position", videoView.getCurrentPosition());
        videoView.pause();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentTimInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentTimInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentTimInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentTimInteraction(Uri uri);
    }

}
